#!/usr/bin/perl

use CGI qw(:standard escapeHTML);
use POSIX qw(strftime);

$ENV{'PATH'} = '/bin:/usr/bin';
$ENV{'LANG'} = 'en_US.UTF-8';

my $command = "/nlformat/nlformat -s -H mail2.fsfe.org ";

my $query = new CGI;
my $remoteUser = $query->remote_user;
my $mailaddress = $remoteUser.'@fsfe.org';
my $mailtype = $query->param("mailtype");
my $includeDonationBanner = $query->param("include-donation-banner");
my $uriordate = $query->param("uriordate");
my $dryrun = $query->param("dryrun");
my $commandline = $command;
my $errorstring = "";
my $longtype = "";
my $drystatus = "No";

my %messagetypes = (
  "nl" => "Newsletter",
  "pr" => "Press release",
);

if (not ($mailtype eq "nl" || $mailtype eq "pr")) {
 $commandline = "invalid";
 $errorstring = '<p>Invalid type of publication!</p>';
} else {
 $longtype = $messagetypes{$mailtype};
}

if (not ($includeDonationBanner eq "include" || $includeDonationBanner eq "exclude")) {
 $commandline = "invalid";
 $errorstring = '<p>Please choose whether to include donation banners (if present on page)!</p>';
}

if ($mailtype eq "nl" and not $uriordate =~ m/^\d{6}$/ ) {

 $commandline = "invalid";
 $errorstring = '<p>Invalid Newsletter date!</p>';

}

if ($commandline ne "invalid" and $dryrun eq "on") {
 $drystatus = "Yes";
 $commandline = $commandline.'-t '.$mailaddress.' ';
}

if ( $commandline ne "invalid" ) {

$commandline = $commandline.$mailtype.' '.$includeDonationBanner.' '.$uriordate.' > /dev/null';
system("$commandline");
print "Content-type: text/html\n\n";
print "<html>";
print "<head><title>Message sent</title></head>";
print "<body>";
print "<h1>Message sent successfully</h1>";
print "<p>";
print "The following command was run as ". (getpwuid($<))[0] .": ".$commandline."<br />\n\n";
print "<button onclick='history.go(-1);'>Go back</button>";
print "</p>";
print "</body>";
print "</html>";

} else {

print "Content-type: text/html\n\n";
print "<html>";
print "<head><title>FAILURE</title></head>";
print "<body>";
print "<h1>Sending message failed</h1>";
print "<p>No message was sent. Somewhere, something went terribly wrong.</p>";
print "<p>Maybe the following error line is helpful:</p>";
print $errorstring;
print "</body>";
print "</html>";

}
