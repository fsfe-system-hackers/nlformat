FROM bitnami/apache:2.4

ARG LDAP_BIND_PW
ARG LDAP_IP

# install required packages
USER 0
RUN install_packages perl python3 libcgi-pm-perl

# Switch back to UID 1001
USER 1001

# Copy nlformat script files to separate directory
COPY nlformat /nlformat

# Configure CGI and HTML files, and Apache
COPY cgi /app/cgi/
COPY www/ /app/

# Enable required modules
RUN sed -i -r -e "s/#LoadModule cgid_module/LoadModule cgid_module/" \
              -e "s/#LoadModule authnz_ldap_module/LoadModule authnz_ldap_module/" \
              -e "s/#LoadModule ldap_module/LoadModule ldap_module/" \
              /opt/bitnami/apache/conf/httpd.conf
COPY nlformat.conf /vhosts/

# Change LDAP bind PW as root, then switch back
USER 0
RUN sed -i -r "s/LDAP_BIND_PW/${LDAP_BIND_PW}/" /vhosts/nlformat.conf
RUN sed -i -r "s/LDAP_IP/${LDAP_IP}/" /vhosts/nlformat.conf
USER 1001
