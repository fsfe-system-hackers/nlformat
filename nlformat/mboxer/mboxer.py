#!/usr/bin/env python3

import os
import sys
import re
import glob
import logging

import concurrent.futures
import urllib.request, urllib.error, urllib.parse
import smtplib

from html.parser import HTMLParser
from textwrap import wrap
from email import charset
from email.message import Message
from email.charset import Charset, QP, SHORTEST
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.nonmultipart import MIMENonMultipart
from email.utils import formatdate

fromaddr = "press@fsfe.org"
replytoaddr = "press@fsfe.org"
organization = "Free Software Foundation Europe"
http_workers = 20


def load_url(url):
    conn = urllib.request.urlopen(url)
    return conn.read().decode("utf-8")


def load_urls(urls):
    urldata = {}
    errored = False
    with concurrent.futures.ThreadPoolExecutor(max_workers=http_workers) as executor:
        future_to_url = {executor.submit(load_url, url): url for url in urls}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                logging.error("%r generated an exception: %s" % (url, exc))
                errored = True
            else:
                urldata[url] = data
                logging.debug("Downloaded %s" % url)

    return (urldata, errored)


def linkme(link):
    if "://" in link:
        return link
    if not link:
        return ""
    if link[0] == "/":
        return "https://fsfe.org" + link
    return "https://fsfe.org/" + link


def urlc(url):
    return re.compile(r"^https?://(www\.)?" + url + r"(\?.*)?(#.*)?$", flags=re.I)


class tagHandlers:

    speciallinks = {
        urlc(r"fsfe\.org"): lambda s, l: s,
        urlc(r"fsfe\.org/news/news\.[a-z]{2}\.rss"): lambda s, l: "%s <%s>" % (s, l),
        urlc(r"fsfe\.org/events/events\.[a-z]{2}\.rss"): lambda s, l: "%s <%s>"
        % (s, l),
        urlc(r"planet\.fsfe\.org/[a-z]{2}/rss20\.xml"): lambda s, l: "%s <%s>" % (s, l),
        urlc(r"fsfe\.org/contact/community\.[a-z]{2}\.html"): lambda s, l: "%s <%s>"
        % (s, l),
        urlc(r"fsfe\.org/about/kirschner/index\.[a-z]{2}\.html"): lambda s, l: s,
    }

    @classmethod
    def handle_h1(self, text, attrs, links, curtext):
        return " = %s =" % text.strip()

    @classmethod
    def handle_h2(self, text, attrs, links, curtext):
        return " == %s ==" % text.strip()

    @classmethod
    def handle_h3(self, text, attrs, links, curtext):
        return " === %s ===" % text.strip()

    @classmethod
    def handle_b(self, text, attrs, links, curtext):
        return "*%s*" % text.strip()

    @classmethod
    def handle_em(self, text, attrs, links, curtext):
        return "/%s/" % text.strip()

    @classmethod
    def handle_a(self, text, attrs, links, curtext):
        if not attrs:
            return text.strip()

        link = None
        for attr in attrs:
            if attr[0] == "href":
                link = str(attr[1])
                break

        if link is None:
            logging.error(
                "No valid href attribute for link around %r"
                ", following this text: %s" % (text, curtext)
            )

        link = linkme(link)

        for r, f in self.speciallinks.items():
            if r.match(link):
                return f(text.strip(), link)

        links.append(link)

        return "%s [%d]" % (text.strip(), len(links))

    @classmethod
    def handle_p(self, text, attrs, links, curtext):
        return text.strip()


class htmlParser(HTMLParser):
    def __init__(self, togen, donate_banner):
        # initialize the base class
        HTMLParser.__init__(self)
        # add the togen argument to the extended class
        self.togen = togen
        self.donate_banner = donate_banner

    data_handlers = {
        "a": tagHandlers.handle_a,
        "em": tagHandlers.handle_em,
        "i": tagHandlers.handle_em,
        "b": tagHandlers.handle_b,
        "strong": tagHandlers.handle_b,
        None: tagHandlers.handle_p,
    }

    endtags_handlers = {
        "h1": tagHandlers.handle_h1,
        "h2": tagHandlers.handle_h2,
        "h3": tagHandlers.handle_h3,
        "li": tagHandlers.handle_p,
        "p": tagHandlers.handle_p,
    }

    INLINETAGS = ["a", "strong", "i", "em", "abbr"]

    NOENDTAGS = ["br", "input", "img", "hr"]

    waitingfor = None

    opentags = []
    curattrs = None
    curtext = ""

    links = []
    out = []
    title = None
    langexists = True

    @classmethod
    def ignoretag(cls, tag, attrs, togen, donate_banner):
        # these tags (and attributes) are ignored across output types
        IGNORETAGS = [
            "form",
            "option",
            "select",
            "input",
            "script",
            "img",
            "noscript",
            "aside",
            "figure",
        ]
        IGNORETAGS_ATTRS = {
            "a": [("href", "#top")],
            "p": [
                ("id", "outdated-notice"),
                ("id", "category"),
                ("class", "warning red"),
                ("class", "hide-in-mail"),
            ],
            "div": [
                ("id", "article-metadata"),
                ("class", "captioned"),
                ("class", "hide-in-mail"),
            ],
            "footer": [("id", "tags")],
        }

        # exclude donation banner
        if donate_banner == "exclude":
            IGNORETAGS_ATTRS["div"].append(("class", "banner-become-supporter"))

        # exlcude discussion link from PRs
        if togen == "pr":
            IGNORETAGS_ATTRS["p"].append(("id", "discussion-link"))

        # ignore tags
        if tag in IGNORETAGS:
            return True

        # ignore attributes
        if tag in IGNORETAGS_ATTRS:
            for attr in attrs:
                if attr in IGNORETAGS_ATTRS[tag]:
                    return True

        return False

    def handle_starttag(self, tag, attrs):
        # start effective parsing when encountering <div id='content'>
        if not self.opentags:
            if ("id", "content") in attrs:  # start analysing!
                del self.out[:]
                del self.links[:]
                self.langexists = True
            else:
                return  # we have not started analysing yet

        # if there is an outdated notice, save this information
        if tag == "p" and ("id", "outdated-notice") in attrs:
            self.langexists = False

        if tag in ("ul", "ol"):
            self.out.append((tag, len(self.links), "+"))

        if tag not in htmlParser.NOENDTAGS:

            # if we have to ignore this tag, let's stop analysing until we're
            # done with it
            if (
                htmlParser.ignoretag(tag, attrs, self.togen, self.donate_banner)
                and not self.waitingfor
            ):
                self.waitingfor = list(self.opentags)

            self.opentags.append(tag)
            self.curattrs = attrs

        elif tag == "br" and self.curtext != "" and "li" not in self.opentags:
            self.out.append((tag, len(self.links), self.curtext.strip()))
            self.curtext = ""
        elif tag == "hr":
            self.out.append((tag, len(self.links), "-" * 68))

    def spaceme(self, text):
        if not text:
            return ""
        return "" if text[0] in " .,:;!?" else " "

    whitespaces = re.compile(r"\s+")

    def handle_data(self, data):
        if not self.opentags or self.waitingfor:
            return

        tag = self.opentags[-1]
        text = self.whitespaces.sub(r" ", data)
        if text.startswith(" "):
            text = " " + text.strip()
        else:
            text = text.strip()

        handlers = htmlParser.data_handlers
        if tag in handlers and not (
            "a" in self.opentags and tag in ("em", "i", "strong")
        ):
            tagtext = handlers[tag](text, self.curattrs, self.links, self.curtext)
            self.curtext += self.spaceme(tagtext) + tagtext
        else:
            self.curtext += self.spaceme(text) + text

    def handle_endtag(self, tag):

        if not self.opentags:
            return

        curattrs = self.curattrs
        self.curattrs = None

        endtag = self.opentags.pop()

        if self.opentags == self.waitingfor:
            self.waitingfor = None
            return

        if self.waitingfor:
            return

        handlers = htmlParser.endtags_handlers
        if tag == "p" and "blockquote" in self.opentags:
            text = handlers[tag](self.curtext, curattrs, self.links, "")
            self.out.append(("blockquote", len(self.links), text))
            self.curtext = ""
        elif tag in handlers and tag not in htmlParser.INLINETAGS:
            text = handlers[tag](self.curtext, curattrs, self.links, "")
            self.out.append((endtag, len(self.links), text))
            if tag == "h1":
                self.title = self.curtext
            self.curtext = ""
        elif tag in ("ul", "ol"):
            self.out.append((tag, len(self.links), "-"))

    def handle_startendtag(self, tag, attrs):
        pass


class MBoxer:

    BASE_NL_URL = "https://fsfe.org/news/nl/nl-%s"
    BASE_PR_URL = "https://fsfe.org/news/%s/news-%s"

    # newlines to be applied (before, after) tag
    TAGSSPACES = {
        "h1": (0, 1),
        "h2": (1, 2),
        "h3": (1, 2),
        "p": (0, 2),
        "li": (1, 1),
        "ul": (0, 1),
        "ol": (0, 1),
        "br": (0, 2),
        "hr": (0, 2),
        "blockquote": (0, 2),
    }

    cached_html = {}
    lists = None
    trans = None

    def __init__(self, togen, donate_banner, ident):

        self.togen = togen
        self.donate_banner = donate_banner

        try:
            if self.togen == "nl":
                if re.match(r"\d{6}", ident):
                    date = ident
                else:
                    m = re.match(r".*nl-(\d{6}).*", ident)
                    date = m.group(1)
                self.baseurl = self.BASE_NL_URL % (date)
            else:
                if re.match(r"\d{8}-\d{2}", ident):
                    date = ident
                else:
                    m = re.match(r".*news-(\d{8}-\d{2}).*", ident)
                    date = m.group(1)
                year = date[:4]
                self.baseurl = self.BASE_PR_URL % (year, date)
            self.baseurl += ".%s.html"

        except AttributeError:
            logging.error("Invalid date or link!")
            sys.exit(1)

        if self.lists is None or self.trans is None:
            self.lists = {}
            self.trans = {}

            p = os.path.join(os.path.dirname(__file__), "..", "lists", self.togen)
            if os.path.isfile(p):
                with open(p, "r") as f:
                    for line in f:
                        address, languages = line.split(":", 1)
                        self.lists[address] = languages.strip().split()

            trans_glob = os.path.join(os.path.dirname(__file__), "..", "trans", "*")
            for p in glob.glob(trans_glob):
                key = os.path.basename(p)
                self.trans[key] = {}
                if os.path.isfile(p):
                    with open(p, "r") as f:
                        for line in f:
                            lang, content = line.split(":", 1)
                            self.trans[key][lang] = content.strip()
                elif os.path.isdir(p):
                    for f in glob.glob(os.path.join(p, "*")):
                        lang = os.path.basename(f)
                        with open(f, "r") as f:
                            self.trans[key][lang] = f.read()

    def gettext(self, name, lang):
        trans = self.trans[name]
        return trans[lang] if lang in trans else trans["en"]

    def html2text(self, html, lang, nosig):
        if self.togen == "pr" and self.donate_banner == "include":
            parser = htmlParser("pr", "include")
        elif self.togen == "pr" and self.donate_banner == "exclude":
            parser = htmlParser("pr", "exclude")
        elif self.togen == "nl" and self.donate_banner == "include":
            parser = htmlParser("nl", "include")
        elif self.togen == "nl" and self.donate_banner == "exclude":
            parser = htmlParser("nl", "exclude")
        else:
            raise Exception("Invalid argument given to `htmlParser` in `html2text`")
        parser.feed(html)

        out = ""
        listtags = []

        # define banner linues
        donate_banner_lines = [
            "Freedom in the information society needs your financial contribution.",
            "Become a supporter now",
        ]

        for tag, nlinks, text in parser.out:

            # remove the second line of the banner that gets mangled into the next line
            # in order to preserver correct linewraps
            if donate_banner_lines[1] in text:
                text = text.replace(donate_banner_lines[1], "")

            if tag == "h1" and self.togen == "nl" and not parser.langexists:
                out += "%s\n\n" % self.gettext("translate", lang)

            indenting = ""
            wrapbegin = ""
            text = text.rstrip()
            if tag in ("ul", "ol"):
                if text == "+":
                    listtags.append([tag, 0])
                    continue
                elif text == "-":
                    listtags.pop()
                    text = ""
            elif tag == "li":
                if not text:
                    continue
                indenting = "    " * (len(listtags) - 1)
                if listtags[-1][0] == "ul":
                    start = "- "
                elif listtags[-1][0] == "ol":
                    listtags[-1][1] += 1
                    start = str(listtags[-1][1]) + ". "
                wrapbegin = " " * len(start) + indenting
                text = start + text
            elif tag == "blockquote":
                indenting = "    "
                wrapbegin = "    "

            if tag not in ("h1", "h2", "h3", "h4", "h5", "h6"):
                lines = wrap(indenting + text, 72, subsequent_indent=wrapbegin)
                text = "\n".join(lines)

            if text == "--" and not nosig:
                text = "-- "
                out += "\n"
            out += "\n" * self.TAGSSPACES[tag][0]
            out += text
            out += "\n" * self.TAGSSPACES[tag][1]

            if tag == "h1":
                readonline = self.gettext("readonline", lang)
                link = linkme(self.langurl(lang))
                out += "\n[ %s: %s ]\n\n" % (readonline, link)

        if self.togen == "nl":
            out += "-- \n"
            out += self.gettext("signature", lang)
            out += "\n\n"

        l = len(str(nlinks))
        form = " %" + str(l) + "d: %s\n"
        for i in range(0, nlinks):
            out += form % (i + 1, parser.links[i])

        if self.togen == "pr":
            out += "\n"
            for l in self.gettext("about", lang).splitlines():
                if l:
                    out += "  %s\n" % l
                else:
                    out += "\n"

        parser.close()
        while out.endswith("\n"):
            out = out[:-1]

        title = parser.title.strip()
        out = out.replace("\n\n\n", "\n\n")

        # replace truncated banner text with full formatted banner
        if self.donate_banner == "include":
            pattern = "Freedom in the information society needs your financial contribution.\n\n (\[\d+\])   "
            replace = """+----------------------------------------------------------------------
| Freedom in the information society needs your financial contribution.
| Become a supporter now \g<1>
+----------------------------------------------------------------------

"""
            out = re.sub(pattern, replace, out)

        return (title, out)

    def langurl(self, lang):
        return self.baseurl % lang

    def get_mail(self, addr, given_to):

        (title, content) = ("", "")
        langs = sorted(self.lists[addr])
        for i, lang in enumerate(langs):
            url = self.langurl(lang)
            html = self.cached_html[url]

            (t, c) = self.html2text(html, lang, i > 0)

            if i > 0:
                title += " | "
            title += t

            if i > 0:
                content += "\n\n" + "-" * 72 + "\n\n"
            content += c

        to = addr
        extra = ""
        if given_to:
            to = given_to
            extra = "[This mail would have been sent to %s]\n\n" % addr

        charset.add_charset("utf-8", SHORTEST, QP)

        ## This is in preparation for sending text/html as well as text/plain
        # msg = MIMEMultipart()

        msg = MIMENonMultipart("text", "plain", charset="utf-8")
        cset = Charset("utf-8")
        cset.body_encoding = QP
        msg["From"] = fromaddr
        msg["Reply-To"] = replytoaddr
        msg["Organization"] = organization
        msg["To"] = to
        msg["Date"] = formatdate(localtime=True)
        msg["Subject"] = title
        msg.set_payload(extra + content, charset=cset)

        ## Switch to this when using MIME multiparts for text/html:
        # textpart = MIMEText(extra+content, 'plain', _charset = cset)
        # msg.attach(textpart)
        return msg

    def fetch_pages(self):
        urls = set()
        for langs in self.lists.values():
            for l in langs:
                urls.add(self.langurl(l))
        missing = [u for u in urls if u not in self.cached_html]

        urldata, errored = load_urls(missing)
        if errored:
            logging.error("Could not download all pages.")
            sys.exit(1)

        logging.debug("All done")
        for url, data in urldata.items():
            self.cached_html[url] = data

    def send_mails(self, smtphost, to):
        self.fetch_pages()
        addrs = sorted(self.lists.keys())
        for addr in addrs:
            msg = self.get_mail(addr, to)

            addr = to if to else addr
            logging.debug("Finished mail for %s", addr)
            mail = msg.as_string()

            smtp = smtplib.SMTP(smtphost)
            smtp.sendmail(fromaddr, [addr], mail)
            smtp.quit()

    def write_mails(self, w, to):
        self.fetch_pages()
        addrs = sorted(self.lists.keys())
        for i, addr in enumerate(addrs):
            msg = self.get_mail(addr, to)

            addr = to if to else addr
            logging.debug("Finished mail for %s", addr)
            mail = msg.as_string()

            w.write(mail)
            if i < len(addrs) - 1:
                w.write("\n\n")
                w.write("=" * 72)
                w.write("\n\n")
