# nlformat

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/nlformat/00_README)

Send FSFE Newsletter and Press Release emails.

## Usage

#### Print to standard output

	nlformat nl YYYYMM|url
	nlformat pr YYYYMMDD-NN|url

#### Send to the mailing lists

	nlformat nl YYYYMM|url --send
	nlformat pr YYYYMMDD-NN|url --send

#### Test by sending the mails to yourself

	nlformat nl YYYYMM|url --to you@fsfe.org
	nlformat pr YYYYMMDD-NN|url --to you@fsfe.org

(--to already implies --send)

## License

```
Copyright (C) 2011 - Nicolas Jean <nicoulas at fsfe dot org>
Copyright (C) 2013-2014 Daniel Martí <mvdan@mvdan.cc>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
